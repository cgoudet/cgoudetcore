import sys
from unittest.mock import patch

import lightgbm as lgb
import numpy as np
import pandas as pd
import pytest
from sklearn.datasets import make_blobs, make_moons
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import ElasticNet, LogisticRegression

from cgoudetcore.learning import (
    ColumnRegressor,
    MultiCategoryRegressor,
    NormalizedRegressor,
    PipelineLinearRegressor,
    TransformRegressor,
    ZeroRegressor,
    compute_shap_values,
    fit_params,
    init_model,
    is_shap_explicable,
    pred_params,
    transform_dataset,
)


class TestColumnRegressor:
    def test_dataframe(self):
        inp = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]})

        reg = ColumnRegressor("b").fit(inp)
        out = reg.predict(inp)

        np.testing.assert_array_equal(out, np.array([4, 5, 6]))

    def test_numpy(self):
        inp = np.arange(6).reshape(2, 3)

        reg = ColumnRegressor(1).fit(inp)
        out = reg.predict(inp)

        np.testing.assert_array_equal(out, np.array([1, 4]))

    def test_repr(self):
        assert str(ColumnRegressor("b")) == "ColumnRegressor(b)"


class TestZeroRegressor:
    def test_dataframe(self):
        inp = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]})

        reg = ZeroRegressor().fit(inp)
        out = reg.predict(inp)

        np.testing.assert_array_equal(out, np.zeros(3))

    def test_repr(self):
        assert str(ZeroRegressor()) == "ZeroRegressor()"


class TestInitModel:
    def test_keyword(self):
        learner = init_model("ColumnRegressor", {"feature": "test_column"})
        assert isinstance(learner, ColumnRegressor)
        assert learner.feature_name_ == ["test_column"]

    def test_wrong_keyword(self):
        with pytest.raises(RuntimeError) as exc:
            init_model("wrong_keyword")
        assert str(exc.value).startswith("Unknown model name. Must be in ")

    @patch(
        "cgoudetcore.learning.importlib.import_module",
        side_effect=ImportError("mocked error"),
    )
    def test_missing_lib(self, import_module_mock):
        with pytest.raises(RuntimeError) as exc:
            print(init_model("LinearRegression"))
        assert (
            str(exc.value)
            == "Please install library sklearn for model LinearRegression"
        )


class TestPredParams:
    def test_lgbm(self):
        model = lgb.LGBMRegressor()
        out = pred_params(model)
        assert out == {"n_jobs": -1}

    def test_other(self):
        model = ColumnRegressor(feature="some_column")
        out = pred_params(model)
        assert not out

    def test_with_normalize(self):
        model = NormalizedRegressor(model_name="LGBMRegressor", reference="reference")
        out = pred_params(model)
        assert out == {"n_jobs": -1}

    def test_with_multi_category(self):
        configs = {"cat1": {"model_name": "LGBMRegressor"}}
        model = MultiCategoryRegressor(configs)
        out = pred_params(model)
        assert out["pred_params"]["cat1"] == {"n_jobs": -1}


class TestFitParams:
    def test_any(self):
        model = ColumnRegressor(feature="some_column")
        out = fit_params([], model)
        assert not out

    def test_lightgbm(self):
        model = lgb.LGBMRegressor()
        out = fit_params(
            ["cat", "no_cat"],
            model,
            X_valid="x_valid",
            y_valid="y_valid",
            w_valid="w_valid",
            categories=["cat"],
        )
        exp = {
            "categorical_feature": [0],
            "eval_sample_weight": ["w_valid"],
            "eval_set": [("x_valid", "y_valid")],
        }
        assert out["callbacks"]
        del out["callbacks"]
        assert out == exp

    @pytest.mark.skipif(
        "xgboost" not in sys.modules, reason="requires python3.10 or higher"
    )
    def test_xgboost(self):
        import xgboost as xgb

        model = xgb.XGBRegressor()
        out = fit_params(
            ["cat", "no_cat"],
            model,
            X_valid="x_valid",
            y_valid="y_valid",
            w_valid="w_valid",
            categories=["cat"],
        )
        exp = {
            "sample_weight_eval_set": ["w_valid"],
            "eval_set": [("x_valid", "y_valid")],
        }
        assert out == exp

    @pytest.mark.skipif(
        "catboost" not in sys.modules, reason="requires python3.10 or higher"
    )
    def test_catboost(self):
        import catboost

        model = catboost.CatBoostRegressor()
        X_valid = pd.DataFrame(
            np.random.randint(10, size=(10, 2)), columns=["first", "second"]
        )
        y_valid = np.zeros(10)
        w_valid = np.ones(10)
        out = fit_params(
            ["first", "second"],
            model,
            X_valid=X_valid,
            y_valid=y_valid,
            w_valid=w_valid,
            categories=["first"],
        )
        exp = {
            "cat_features": [0],
            "eval_set": catboost.Pool(
                X_valid, y_valid, weight=w_valid, cat_features=[0]
            ),
        }
        assert out == exp

    def test_linear_regressor(self):
        model = PipelineLinearRegressor()
        out = fit_params(
            ["cat", "no_cat"],
            model,
            X_valid="x_valid",
            y_valid="y_valid",
            w_valid="w_valid",
            categories=["cat"],
        )
        exp = {"categories": ["cat"]}
        assert out == exp

    def test_transform_regressor_with_drop_reference(self):
        inp = pd.DataFrame({"x": [0, 1, 2], "reference": [4, 5, 6], "y": [7, 8, 9]})

        model = NormalizedRegressor(
            model_name="LGBMRegressor",
            reference="reference",
            drop_reference=True,
        )
        out = fit_params(
            ["cat", "no_cat"],
            model,
            X_valid=inp[["x", "reference"]],
            y_valid=inp["y"],
            w_valid=None,
            categories=["cat"],
        )
        pd.testing.assert_frame_equal(out["eval_set"][0][0], inp[["x"]])
        pd.testing.assert_series_equal(
            out["eval_set"][0][1], inp["y"].div(inp["reference"])
        )
        assert out["eval_sample_weight"][0] is None

    def test_transform_regressor_without_drop_reference(self):
        inp = pd.DataFrame(
            {
                "x": [0, 1, 2],
                "reference": [4, 5, 6],
                "y": [7, 8, 9],
                "weight": [2, 3, 4],
            }
        )

        model = NormalizedRegressor(
            model_name="LGBMRegressor",
            reference="reference",
            drop_reference=False,
        )
        out = fit_params(
            ["cat", "no_cat"],
            model,
            X_valid=inp[["x", "reference"]],
            y_valid=inp["y"],
            w_valid=inp["weight"],
            categories=["cat"],
        )
        pd.testing.assert_frame_equal(out["eval_set"][0][0], inp[["x", "reference"]])
        pd.testing.assert_series_equal(
            out["eval_set"][0][1], inp["y"].div(inp["reference"])
        )
        pd.testing.assert_series_equal(
            out["eval_sample_weight"][0], inp["weight"] * inp["reference"]
        )

    def test_transform_regressor_without_eval_set(self):
        model = NormalizedRegressor(
            model_name="LGBMRegressor",
            reference="reference",
            drop_reference=False,
        )
        out = fit_params(
            ["cat", "no_cat"],
            model,
            categories=["cat"],
        )
        assert out["eval_set"][0] == (None, None)

    def test_other_model(self):
        model = ElasticNet()
        out = fit_params(
            ["cat", "no_cat"],
            model,
            X_valid="x_valid",
            y_valid="y_valid",
            w_valid="w_valid",
            categories=["cat"],
        )
        assert not out

    def test_multi_category_regressor(self):
        inp = pd.DataFrame(
            {"x": [0, 1, 2], "reference": [4, 5, 6], "y": [7, 8, 9], "cat1": [1, 1, 0]}
        )

        configs = {"cat1": {"model_name": "LGBMRegressor"}}
        model = MultiCategoryRegressor(configs)
        out = fit_params(["x"], model, inp[["x", "cat1"]], inp["y"])
        pd.testing.assert_frame_equal(
            out["fit_params"]["cat1"]["eval_set"][0][0], inp[["x"]].iloc[:2]
        )


def test_transform_regressor_default_inverse():
    inp = pd.DataFrame(
        {"feature": [1, 2, 3], "target": [4, 5, 6], "reference": [7, 8, 9]}
    )

    model = TransformRegressor(
        model_name="ColumnRegressor",
        reference="reference",
        model_opt={"feature": "feature"},
        drop_reference=True,
    ).fit(inp[["feature", "reference"]], inp["target"])

    preds = model.predict(inp[["feature", "reference"]])
    exp = np.array([1, 2, 3])
    np.testing.assert_array_equal(preds, exp)


class TestNormalizeRegressor:
    def test_basic_usage(self):
        inp = pd.DataFrame(
            {"feature": [1, 2, 3], "target": [4, 5, 6], "reference": [7, 8, 9]}
        )

        model = NormalizedRegressor(
            model_name="ColumnRegressor",
            reference="reference",
            model_opt={"feature": "feature"},
            drop_reference=True,
        ).fit(inp[["feature", "reference"]], inp["target"])

        preds = model.predict(inp[["feature", "reference"]])
        exp = np.array([7, 16, 27])
        np.testing.assert_array_equal(preds, exp)

    @patch("cgoudetcore.learning.ColumnRegressor.fit")
    def test_with_sample_weight(self, mock_fit):
        inp = pd.DataFrame(
            {"feature": [1, 2, 3], "target": [4, 5, 6], "reference": [7, 8, 9]}
        )

        NormalizedRegressor(
            model_name="ColumnRegressor",
            reference="reference",
            model_opt={"feature": "feature"},
        ).fit(
            inp[["feature", "reference"]],
            inp["target"],
            sample_weight=np.ones(len(inp)),
        )

        _, kwargs = mock_fit.call_args
        np.testing.assert_array_equal(kwargs["sample_weight"], [7, 8, 9])

    def test_with_zero_reference_should_raise_assert(self):
        inp = pd.DataFrame(
            {"feature": [1, 2, 3], "target": [4, 5, 6], "reference": [7, 0, 9]}
        )

        with pytest.raises(AssertionError):
            NormalizedRegressor(
                model_name="ColumnRegressor",
                reference="reference",
                model_opt={"feature": "feature"},
            ).fit(
                inp[["feature", "reference"]],
                inp["target"],
                sample_weight=np.ones(len(inp)),
            )

    def test_repr(self):
        reg = NormalizedRegressor(
            model_name="ColumnRegressor",
            reference="reference",
            model_opt={"feature": "feature"},
        )
        assert (
            str(reg)
            == "NormalizedRegressor(model_name=ColumnRegressor, reference=reference, model_opt={'feature': 'feature'})"
        )


class TestPipelineLinearRegressor:
    def test_onehot_columns(self):
        """
        Create a perfect target = -1 + 6cont0 + 8cat0
        """
        cont0 = np.random.rand(1000)
        cat = np.random.choice([5, -3], size=(1000))
        target = 2 + 6 * cont0 + cat
        inp = pd.DataFrame({"cat": cat, "cont0": cont0, "target": target})
        # sklearn is going to throw a warning
        # about alpha being 0 and numerical instability.
        # This is expected, we keep alpha=0 to make it easier to check
        mdl = PipelineLinearRegressor({"alpha": 0}, {"drop": "first"})
        assert mdl.model is None

        out = mdl.fit(inp[["cont0", "cat"]], inp["target"], categories=["cat"]).predict(
            inp[["cont0", "cat"]]
        )
        np.testing.assert_allclose(out, inp["target"], rtol=1e-1)
        np.testing.assert_allclose(mdl.model.intercept_, -1, rtol=1e-3)
        np.testing.assert_allclose(mdl.model.coef_, [8, 6], rtol=1e-3)


class TestMultiCategoryRegressor:
    def test_with_one_category_and_uncomplete_coverage(self):
        inp = pd.DataFrame(
            {
                "feature1": [1, 2, 3],
                "feature2": [4, 5, 6],
                "feature3": [7, 8, 8],
                "cat1": [1, 1, 0],
            }
        )
        configs = {
            "cat1": {"model_name": "ColumnRegressor", "opts": {"feature": "feature1"}},
        }

        reg = MultiCategoryRegressor(configs).fit(
            inp[["feature1", "feature2", "cat1"]], inp["feature3"]
        )

        preds = reg.predict(inp[["feature1", "feature2", "cat1"]])
        np.testing.assert_array_equal(preds, [1, 2, np.nan])

    def test_with_one_category_and_remaining(self):
        inp = pd.DataFrame(
            {
                "feature1": [1, 2, 3],
                "feature2": [4, 5, 6],
                "feature3": [7, 8, 8],
                "cat1": [1, 1, 0],
            }
        )
        configs = {
            "cat1": {"model_name": "ColumnRegressor", "opts": {"feature": "feature1"}},
            "remaining_": {
                "model_name": "ColumnRegressor",
                "opts": {"feature": "feature2"},
            },
        }

        reg = MultiCategoryRegressor(configs).fit(
            inp[["feature1", "feature2", "cat1"]], inp["feature3"]
        )
        preds = reg.predict(inp[["feature1", "feature2", "cat1"]])
        np.testing.assert_array_equal(preds, [1, 2, 6])

    def test_ensure_fit_params_are_passed_to_lgbm(self):
        inp = pd.DataFrame(
            {"x": [0, 1, 2], "reference": [4, 5, 6], "y": [7, 8, 9], "cat1": [1, 1, 0]}
        )

        configs = {
            "cat1": {"model_name": "LGBMRegressor", "opts": {"n_jobs": 1}},
        }
        reg = MultiCategoryRegressor(configs)

        with patch("lightgbm.LGBMRegressor.fit") as mock_fit:
            reg.fit(
                inp[["x", "cat1"]],
                inp["y"],
                fit_params={"cat1": {"unknown_parameter": True}},
            )

            arguments = mock_fit.call_args_list
            pd.testing.assert_frame_equal(arguments[0][0][0], inp[["x"]].iloc[:2])
            assert arguments[0].unknown_parameter

    def test_ensure_pred_params_are_passed(self):
        inp = pd.DataFrame(
            {"x": [0, 1, 2], "reference": [4, 5, 6], "y": [7, 8, 9], "cat1": [1, 1, 0]}
        )

        configs = {
            "cat1": {"model_name": "LGBMRegressor", "opts": {"n_jobs": 1}},
        }
        reg = MultiCategoryRegressor(configs)
        reg.fit(inp[["x", "cat1"]], inp["y"])

        with patch("lightgbm.LGBMRegressor.predict") as mock_predict:
            mock_predict.return_value = [1, 2]
            reg.predict(
                inp[["x", "cat1"]], pred_params={"cat1": {"unknown_parameter": True}}
            )

            arguments = mock_predict.call_args_list
            pd.testing.assert_frame_equal(arguments[0][0][0], inp[["x"]].iloc[:2])
            assert arguments[0].unknown_parameter


def test_transform_dataset():
    inp = pd.DataFrame(make_moons(random_state=100)[0], columns=["first", "second"])

    out = transform_dataset(inp, "PCA", {"n_components": 1, "random_state": 4})

    expected = pd.DataFrame(
        PCA(n_components=1, random_state=4).fit_transform(inp), columns=["emb_0"]
    )
    pd.testing.assert_frame_equal(out, expected)


class TestIsShapEplicable:
    def test_is_shap_explicable(self):
        data, y = make_blobs(centers=2)

        explicable = RandomForestClassifier(n_estimators=2).fit(data, y)

        assert is_shap_explicable(explicable)

    def test_is_not_shap_explicable(self):
        data, y = make_blobs(centers=2)

        non_explicable = LogisticRegression().fit(data, y)

        assert not is_shap_explicable(non_explicable)


def test_shap_values():
    data, y = make_blobs(centers=2, random_state=45)
    data = pd.DataFrame(data)

    learner = RandomForestClassifier(n_estimators=2, random_state=12).fit(data, y)

    shap_values = compute_shap_values(learner, data)
    np.testing.assert_array_almost_equal(
        shap_values[0][:2], [[0.24, -0.24], [0.22, -0.21]], decimal=2
    )
    np.testing.assert_array_almost_equal(
        shap_values[1][:2], [[-0.26, 0.26], [-0.28, 0.29]], decimal=2
    )
