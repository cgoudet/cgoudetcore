import json
import os
import tempfile
from pathlib import Path

import joblib
import numpy as np
import pandas as pd
import pytest
from sklearn.ensemble import RandomForestClassifier

from cgoudetcore.utils import (
    addl,
    categorify_columns,
    category_to_json,
    check_missing_cols,
    check_missings,
    diffl,
    flatten_multiindex,
    json_categorify,
    multi_replace,
    pickle_size,
    read_external,
    reduce_float_type,
    reduce_int_type,
)


def test_category_to_json():
    inp = pd.Series(["b", "a", "c", "b"])
    out = category_to_json(inp)
    exp = {"a": 0, "b": 1, "c": 2}
    assert out == exp


def test_reduce_float_type():
    inp = pd.DataFrame({"col": [1.1, 5.2, 1e8]})

    out = reduce_float_type(inp)
    pd.testing.assert_frame_equal(out, inp.astype(np.float32))


def test_reduce_int_type():
    inp = pd.DataFrame(
        {
            "int8": [-1, 126],
            "int16": [-1000, 31000],
            "int32": [-35000, 35000],
            "int64": [-21474836489, 21474836489],
        }
    )
    out = reduce_int_type(inp)
    pd.testing.assert_frame_equal(
        out, inp.astype({"int8": np.int8, "int16": np.int16, "int32": np.int32})
    )


class TestCheckMissing:
    def test_no_missing_dataframe_ok(self):
        inp = pd.DataFrame({"col0": [1, 2], "col1": [3, 4]})
        check_missings(inp, ["col0", "col1"])

    def test_no_missing_series_ok(self):
        inp = pd.Series([1, 2])
        check_missings(inp)

    def test_with_missing_data_in_frame_should_raise_error(self):
        inp = pd.DataFrame({"col0": [1, np.nan], "col1": [3, 4]})
        with pytest.raises(RuntimeError) as excinfo:
            check_missings(inp, ["col0", "col1"])
        assert str(excinfo.value) == "missing values"

    def test_with_missing_data_auto_all_columns(self):
        inp = pd.DataFrame({"col0": [1, np.nan], "col1": [3, 4]})
        with pytest.raises(RuntimeError) as excinfo:
            check_missings(inp)
        assert str(excinfo.value) == "missing values"

    def test_with_missing_data_in_serie_should_raise_error(self):
        inp = pd.Series([1, np.nan])
        with pytest.raises(RuntimeError) as excinfo:
            check_missings(inp)
        assert str(excinfo.value) == "missing values"


def test_pickle_size():
    assert pickle_size([1, 2]) == 20


class TestFlattenMultiIndex:
    def test_simple_index_should_return_same(self):
        inp = pd.Index(["a", "b", "c"])
        pd.testing.assert_index_equal(flatten_multiindex(inp), inp)

    def test_multi_index_should_return_joined(self):
        inp = pd.MultiIndex.from_tuples(
            [("a", "b"), ("b", "c")], names=["test", "names"]
        )

        out = flatten_multiindex(inp, sep="__")
        exp = pd.Index(["a__b", "b__c"], name="test__names")
        pd.testing.assert_index_equal(out, exp)

    def test_multiindex_wihtout_names_should_return_without_names(self):
        inp = pd.MultiIndex.from_tuples([("a", "b"), ("b", "c")])

        out = flatten_multiindex(inp)
        exp = pd.Index(["a_b", "b_c"])
        pd.testing.assert_index_equal(out, exp)


class TestutilsAddl:
    def test_standard(self):
        l1 = ["a", "d", "e"]
        l2 = ["b", "f", "c"]
        assert addl(l1, l2) == list("abcdef")

    def test_l2empty(self):
        l1 = ["a", "d", "e"]
        assert addl(l1, []) == list("ade")

    def test_l1empty(self):
        l2 = ["a", "z", "e"]
        assert addl([], l2) == list("aez")

    def test_both_empty(self):
        assert not addl([], [])


class TestDiffL:
    def test_no_common(self):
        l1 = ["a", "z", "e"]
        l2 = ["b", "f", "c"]
        assert diffl(l1, l2) == list("aez")

    def test_all_common(self):
        l1 = ["a", "z", "e"]
        l2 = ["z", "e", "a"]
        assert not diffl(l1, l2)

    def test_some_common(self):
        l1 = ["a", "z", "e"]
        l2 = ["z", "b", "a"]
        assert diffl(l1, l2) == ["e"]

    def test_l2empty(self):
        l1 = ["a", "z", "e"]
        assert diffl(l1, []) == list("aez")

    def test_l1empty(self):
        l2 = ["a", "z", "e"]
        assert not diffl([], l2)

    def test_both_empty(self):
        assert not diffl([], [])


class TestCheckMissingCols:
    def test_missing(self):
        inp = pd.DataFrame([[1, 2], [3, 4]], columns=["col0", "col1"])
        with pytest.raises(KeyError):
            check_missing_cols(inp, ["col0", "missing_cols"])

    def test_ok(self):
        inp = pd.DataFrame([[1, 2], [3, 4]], columns=["col0", "col1"])
        check_missing_cols(inp, ["col0", "col1"])


class TestMultiReplace:
    def test_standard(self):
        replaced = multi_replace("aaa bbb ccc", {"aaa": "ddd", "ccc": "eee"})
        assert replaced == "ddd bbb eee"

    def test_long_short_replace(self):
        replaced = multi_replace("aaa bbb ccc", {"aaa": "ddd", "aa": "ee"})
        assert replaced == "ddd bbb ccc"

    def test_no_matching(self):
        replaced = multi_replace("aaa bbb ccc", {"ddd": "eee"})
        assert replaced == "aaa bbb ccc"

    def test_case_sensitive(self):
        replaced = multi_replace("AAA bbb ccc", {"aaa": "eee", "AAA": "ddd"})
        assert replaced == "ddd bbb ccc"

    def test_empty_replacement(self):
        replaced = multi_replace("AAA bbb ccc", {})
        assert replaced == "AAA bbb ccc"

    def test_empty_input(self):
        replaced = multi_replace("", {"a": "b"})
        assert replaced == ""


class TestReadExternal:
    def test_list(self):
        d = {"b": ["a", "c"]}
        out = read_external(d, "b")
        assert out == ["a", "c"]

    def test_joblib(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            fn = Path(tmpdirname) / "test_joblib.joblib"
            clf = RandomForestClassifier()
            clf.features = ["a", "b", "c"]
            joblib.dump(clf, fn)
            d = {"a": 1, "b": fn}

            out = read_external(d, "b")
            assert out == ["a", "b", "c"]

    def test_dict(self):
        d = {"a": 1, "b": {"c": 2}}
        out = read_external(d, "b")
        assert out == {"c": 2}

    def test_json(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            fn = os.path.join(tmpdirname, "test_json.json")
            with open(fn, "w") as f:
                json.dump({"c": 2}, f)
            d = {"a": 1, "b": fn}

            out = read_external(d, "b")
            assert out == {"c": 2}

    def test_txt(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            fn = Path(tmpdirname) / "test_txt.txt"
            np.savetxt(fn, ["1", "b", "c"], fmt="%s")
            d = {"a": 1, "b": fn}

            out = read_external(d, "b")
            assert out == ["1", "b", "c"]

    def test_wrong_format(self):
        with pytest.raises(RuntimeError) as excinfo:
            read_external({"b": 5}, "b")
        assert str(excinfo.value) == "Wrong input format : <class 'int'>"

    def test_missing_key_should_return_none(self):
        assert read_external({"b": 5}, "c") is None

    def test_should_raise_error_if_unknown_extension(self):
        with pytest.raises(RuntimeError) as excinfo:
            read_external({"b": Path("/absolute/test.unknown")}, "b")
        assert str(excinfo.value) == "Unknown extension : .unknown"


class TestJSONCategorify:
    def test_dataframe_no_json_present(self):
        inp = pd.DataFrame({"category": ["maison", "cheval", "maison"]})
        with tempfile.TemporaryDirectory() as tmpdirname:
            out = json_categorify(inp, "category", Path(tmpdirname))
            expected = pd.DataFrame({"category": [1, 0, 1]})
            pd.testing.assert_frame_equal(out, expected)

            with open(os.path.join(tmpdirname, "mapping_category.json")) as f:
                mapping = json.load(f)
                assert mapping == {"cheval": 0, "maison": 1}

    def test_dataframe_with_previous_json(self):
        inp = pd.DataFrame({"category": ["maison", "cheval", "maison"]})
        with tempfile.TemporaryDirectory() as tmpdirname:
            with open(os.path.join(tmpdirname, "mapping_category.json"), "w") as f:
                json.dump({"unknown": 0, "maison": 1}, f)

            out = json_categorify(inp, "category", Path(tmpdirname))
            expected = pd.DataFrame({"category": [1, -1, 1]})
            pd.testing.assert_frame_equal(out, expected)

    def test_dataframe_with_null_values(self):
        inp = pd.DataFrame({"category": ["maison", "cheval", None]})
        with tempfile.TemporaryDirectory() as tmpdirname:
            out = json_categorify(inp, "category", Path(tmpdirname))
        expected = pd.DataFrame({"category": [1, 0, -1]})
        pd.testing.assert_frame_equal(out, expected)

    def test_dataframe_with_multiple_columns(self):
        inp = pd.DataFrame(
            {
                "category": ["maison", "cheval", "maison"],
                "nom": ["pierre", "paul", "jacques"],
            }
        )
        with tempfile.TemporaryDirectory() as tmpdirname:
            out = categorify_columns(inp, ["category", "nom"], Path(tmpdirname))
            expected = pd.DataFrame({"category": [1, 0, 1], "nom": [2, 1, 0]})
            pd.testing.assert_frame_equal(out, expected)

            with open(os.path.join(tmpdirname, "mapping_category.json")) as f:
                mapping = json.load(f)
                assert mapping == {"cheval": 0, "maison": 1}

            with open(os.path.join(tmpdirname, "mapping_nom.json")) as f:
                mapping = json.load(f)
                assert mapping == {"pierre": 2, "paul": 1, "jacques": 0}
