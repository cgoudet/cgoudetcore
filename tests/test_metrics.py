import numpy as np

from cgoudetcore.metrics import rmse, rmsle


def test_rmsle_single():
    assert rmsle(np.array([10]), np.array([5])) == 0.6061358035703157


def test_rmsle_multiple():
    np.testing.assert_almost_equal(
        rmsle(np.array([10, 0]), np.array([5, 3])), 1.0698627641063685
    )


def test_rmse_multiple():
    np.testing.assert_almost_equal(
        rmse(np.array([10, 9]), np.array([1, 5])), 6.96419413859206
    )
