import numpy as np
from sklearn.datasets import make_blobs

from cgoudetcore.visualisation.cluster import (
    ScatterNDim,
    draw_clusters,
    draw_silhouette,
    draw_zscatter,
)


class TestScatterNDim:
    def test_draw_in_2D(self):
        data, _ = make_blobs(n_samples=100, centers=3, n_features=2, random_state=0)

        plotter = ScatterNDim()
        plotter.draw(data, dimensions_name=["first", "second"])

        out_data = plotter.ax.collections[0].get_offsets().data
        np.testing.assert_array_equal(out_data, data)

    def test_draw_clusters_in_3d(self):
        data, _ = make_blobs(n_samples=100, centers=2, n_features=3, random_state=0)
        plotter = ScatterNDim()
        plotter.draw(data)

        top_left_data = plotter.ax[0][0].collections[0].get_offsets().data
        np.testing.assert_array_equal(top_left_data, data[:, [0, 1]])

        top_right_data = plotter.ax[0][1].collections[0].get_offsets().data
        np.testing.assert_array_equal(top_right_data, data[:, [0, 2]])

        bottom_right_data = plotter.ax[1][1].collections[0].get_offsets().data
        np.testing.assert_array_equal(bottom_right_data, data[:, [1, 2]])


def test_draw_silhouette():
    data, labels = make_blobs(n_samples=100, centers=3, n_features=2, random_state=0)
    _, ax = draw_silhouette(data, labels)
    assert len(ax.collections) == 3


def test_draw_clusters():
    data, labels = make_blobs(n_samples=100, centers=3, n_features=2, random_state=0)
    _, ax = draw_clusters(data, labels)

    out_data = ax.collections[0].get_offsets().data

    np.testing.assert_array_equal(out_data, data[labels == 0])


def test_draw_zscatter():
    data, labels = make_blobs(n_samples=100, centers=3, n_features=2, random_state=0)
    _, ax = draw_zscatter(data, labels)

    out_data = ax.collections[0].get_offsets().data

    np.testing.assert_array_equal(out_data, data)
