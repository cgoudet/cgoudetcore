from unittest.mock import patch

import numpy as np
import pandas as pd
from sklearn.datasets import make_blobs
from sklearn.ensemble import RandomForestClassifier

from cgoudetcore.visualisation.explicability import (
    GroupPerformancePlot,
    linear_coef_plot,
    shap_plot,
)


@patch("cgoudetcore.visualisation.explicability.shap.summary_plot")
def test_shap_plot(mock_plot):
    data, y = make_blobs(centers=2, random_state=45)
    data = pd.DataFrame(data)

    learner = RandomForestClassifier(n_estimators=2, random_state=12).fit(data, y)

    shap_plot(learner, data.iloc[:2])

    args, _ = mock_plot.call_args

    np.testing.assert_array_almost_equal(
        args[0][0], [[0.24, -0.24], [0.22, -0.21]], decimal=2
    )
    np.testing.assert_array_almost_equal(
        args[0][1], [[-0.26, 0.26], [-0.28, 0.29]], decimal=2
    )
    pd.testing.assert_frame_equal(args[1], data.iloc[:2])


def test_linear_coef_plot():
    coefs = pd.Series([1, 2, 3], index=["a", "b", "c"])
    _, ax = linear_coef_plot(coefs)
    assert [rect.get_width() for rect in ax.patches] == [3, 2, 1]


class TestGroupPerformance_plot:
    def test_with_color_category(self):
        df = pd.DataFrame(
            {
                "baseline": [1, 2, 3, 4, 5],
                "preds": [2, 3, 4, 5, 6],
                "color": ["a", "a", "b", "b", "b"],
            },
            index=pd.Index(["a", "b", "c", "d", "e"], name="site"),
        )
        _, ax = GroupPerformancePlot(color_col="color").plot(df)
        assert len(ax.collections) == 2
        assert ax.get_xlabel() == "site"

    def test_without_color_category_and_multiIndex(self):
        df = pd.DataFrame(
            {
                "baseline": [1, 2, 3, 4, 5],
                "preds": [2, 3, 4, 5, 6],
            },
            index=pd.MultiIndex.from_tuples(
                [(1, "a"), (2, "b"), (3, "c"), (4, "d"), (5, "e")],
                names=["num", "site"],
            ),
        )
        _, ax = GroupPerformancePlot().plot(df)
        assert len(ax.collections) == 1
        assert ax.get_xlabel() == "num; site"
