import itertools

import numpy as np
import pandas as pd

from cgoudetcore.visualisation.timeseries import SplittedSeriePlotter


class TestSplittedSeries_plots:
    def test_with_2_splits_and_single_feature(self):
        dt = pd.date_range("2023-01-01", "2023-01-07")

        inp = pd.concat(
            [
                pd.DataFrame(
                    {
                        "date": dt,
                        "company": company,
                        "category": category,
                        "quantity": np.arange(7) * company + category,
                    }
                )
                for company, category in itertools.product([1, 2], [3, 4])
            ]
        )
        _, ax = SplittedSeriePlotter(
            "date",
            features=["quantity"],
            vertical_split_column="company",
            horizontal_split_column="category",
        ).plot(inp)
        np.testing.assert_array_equal(
            ax[0][0].lines[0].get_xydata()[:, 1], np.arange(7) + 3
        )
        np.testing.assert_array_equal(
            ax[0][1].lines[0].get_xydata()[:, 1], np.arange(7) + 4
        )
        np.testing.assert_array_equal(
            ax[1][0].lines[0].get_xydata()[:, 1], 2 * np.arange(7) + 3
        )
        np.testing.assert_array_equal(
            ax[1][1].lines[0].get_xydata()[:, 1], 2 * np.arange(7) + 4
        )

    def test_with_2_segments_of_1_elements(self):
        dt = pd.date_range("2023-01-01", "2023-01-07")

        inp = pd.concat(
            [
                pd.DataFrame(
                    {
                        "date": dt,
                        "company": company,
                        "category": category,
                        "quantity": np.arange(7) * company + category,
                    }
                )
                for company, category in itertools.product([1], [3])
            ]
        )
        _, ax = SplittedSeriePlotter(
            "date",
            features=["quantity"],
            vertical_split_column="company",
            horizontal_split_column="category",
        ).plot(inp)
        np.testing.assert_array_equal(ax.lines[0].get_xydata()[:, 1], np.arange(7) + 3)

    def test_with_1_vertical_segment_and_single_feature(self):
        dt = pd.date_range("2023-01-01", "2023-01-07")

        inp = pd.concat(
            [
                pd.DataFrame(
                    {"date": dt, "company": company, "quantity": np.arange(7) * company}
                )
                for company in [1, 2]
            ]
        )
        _, ax = SplittedSeriePlotter(
            "date",
            features=["quantity"],
            vertical_split_column="company",
        ).plot(inp)
        np.testing.assert_array_equal(ax[0].lines[0].get_xydata()[:, 1], np.arange(7))
        np.testing.assert_array_equal(
            ax[1].lines[0].get_xydata()[:, 1], 2 * np.arange(7)
        )

    def test_with_1_horizontal_segment_and_single_feature(self):
        dt = pd.date_range("2023-01-01", "2023-01-07")

        inp = pd.concat(
            [
                pd.DataFrame(
                    {"date": dt, "company": company, "quantity": np.arange(7) * company}
                )
                for company in [1, 2]
            ]
        )
        _, ax = SplittedSeriePlotter(
            "date",
            features=["quantity"],
            horizontal_split_column="company",
        ).plot(inp)
        np.testing.assert_array_equal(ax[0].lines[0].get_xydata()[:, 1], np.arange(7))
        np.testing.assert_array_equal(
            ax[1].lines[0].get_xydata()[:, 1], 2 * np.arange(7)
        )

    def test_with_no_split_and_single_feature(self):
        dt = pd.date_range("2023-01-01", "2023-01-07")

        inp = pd.DataFrame({"date": dt, "quantity": np.arange(7)})
        _, ax = SplittedSeriePlotter(
            "date",
            features=["quantity"],
        ).plot(inp)
        np.testing.assert_array_equal(ax.lines[0].get_xydata()[:, 1], np.arange(7))

    def test_with_no_split_and_multiple_features(self):
        dt = pd.date_range("2023-01-01", "2023-01-07")

        inp = pd.DataFrame(
            {"date": dt, "quantity": np.arange(7), "quantity1": 2 * np.arange(7)}
        )
        _, ax = SplittedSeriePlotter(
            "date",
            features=["quantity", "quantity1"],
        ).plot(inp)
        np.testing.assert_array_equal(ax.lines[0].get_xydata()[:, 1], np.arange(7))
        np.testing.assert_array_equal(ax.lines[1].get_xydata()[:, 1], 2 * np.arange(7))
