import itertools
import json
import re
from functools import partial
from typing import Any

import category_encoders as ce
import numpy as np
import pandas as pd
from fastcore.utils import listify, store_attr
from scipy.stats import spearmanr
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from umap import UMAP

from .utils import flatten_multiindex


class GeneralEncoder(BaseEstimator, TransformerMixin):
    def __init__(self, extra=[], **kwargs):
        tfs = {
            "onehot": OneHotEncoder,
            "standard": StandardScaler,
            "ordinal": ce.ordinal.OrdinalEncoder,
            "targenc": ce.target_encoder.TargetEncoder,
            "loggifier": Loggifier,
            "passthrough": lambda x=None: "passthrough",
            "date_featurer": DateFeaturer,
        }
        store_attr()
        self.transformer_ = ColumnTransformer(
            [
                (k, v(**kwargs.get(k + "_opt", {})), list(kwargs.get(k, [])))
                for k, v in tfs.items()
            ]
            + extra,
            remainder="drop",
            n_jobs=-1,
        )

    def fit(self, X, y=None):
        self.transformer_.fit(X, y)
        return self

    def transform(self, X, y=None):
        transformed = np.float32(self.transformer_.transform(X))
        if isinstance(X, pd.DataFrame):
            transformed = pd.DataFrame(transformed, columns=self.get_feature_names())
        return transformed

    def get_feature_names(self):
        return list(
            itertools.chain(
                *[
                    self._transformer_names(transformer)
                    for transformer in self.transformer_.transformers_
                ]
            )
        )

    def _transformer_names(self, pipeline_step: tuple[str, Any, list[str]]):
        name, transformer, cols = pipeline_step
        if name == "remainder" and transformer == "drop":
            return []
        if name == "passthrough":
            return cols
        return self._ensure_list(self._transformer_features_name(pipeline_step))

    def _ensure_list(self, names: Any) -> list[str]:
        if isinstance(names, np.ndarray):
            return names.tolist()
        elif isinstance(names, str):
            return [names]
        return names

    def _transformer_features_name(self, pipeline_step: tuple[str, Any, list[str]]):
        name, transformer, columns = pipeline_step
        try:
            names = transformer.get_feature_names_out()
            if isinstance(transformer, OneHotEncoder):
                for i, name in enumerate(columns):
                    pat = re.compile(rf"^x{i}")
                    return [pat.sub(name, x) for x in names]
            else:
                return [f"{name}_{c}" for c in names]
        except ValueError:
            return []
        except (
            AttributeError
        ):  # if no 'get_feature_names' function, use raw column name
            if transformer == "passthrough":
                return columns
            else:
                return [name + "_" + x for x in columns]


class Loggifier(BaseEstimator, TransformerMixin):
    def __init__(self, columns=None, zero_to="min", errors="raise"):
        """Add log of some columns in dataset.

        Keyword Arguments:
        df -- dataset to transform
        columns -- subset of columns to transform
        zero_to -- {'min', 'min1', 'zero', 'raise'}
        - if 'min' : zeros are transformed to the minimum positive value, before taking the log
        - if 'min1' : zeros are transformed to 0.1 times the minimum value of the column, before taking the log10.
        - if 'zero' : zeros are mapped to 0
        - if 'raise' : raise ValueError if zeros are present.

        errors -- {'raise', 'zero', 'abs', 'sign'}
        - if 'raise', raise a ValueError in case of negative value.
        - if 'zero', replace negative values with zero then apply transformation
        - if 'abs', replace negative values with their opposite
        - if 'sign', set all values between [-1, 1] to 0 and set negative log to signed log of absolute value

        Initial columns are conserved
        """
        assert errors in {"raise", "zero", "abs", "sign"}
        assert zero_to in {"min", "min1", "zero", "raise"}
        store_attr()

    def fit(self, X, y=None):
        if isinstance(X, pd.DataFrame) and not self.columns:
            self.columns = list(X.columns)
        return self

    def _check_errors(self, X):
        if self.errors == "raise":
            negatives = (X < 0).sum(axis=0).sum()
            if negatives > 0:
                raise ValueError(f"Negative valuess : {negatives}")

        if self.zero_to == "raise":
            zeros = X == 0
            if self.errors == "zero":
                zeros |= X < 0
            zeros = zeros.sum(axis=1).sum()
            if zeros:
                raise ValueError(f"0 in future log columns : {zeros}")

    def transform(self, X, y=None):
        X = X[self.columns].to_numpy()
        self._check_errors(X)

        if self.errors == "abs":
            X = np.abs(X)
        elif self.errors == "zero":
            X = np.maximum(X, 0)
        elif self.errors == "sign":
            X = np.where(np.abs(X) > 1, X, 1)
            self.zero_to = "zero"
        mask = X == 0

        m = np.nanmin(np.where(mask, np.nan, X), axis=0)
        m[np.isnan(m)] = 1
        X = np.where(mask, m, X)
        if self.errors == "sign":
            X = np.sign(X) * np.log10(np.abs(X))
        else:
            X = np.log10(X)

        if self.zero_to == "zero":
            X = np.where(mask, 0, X)
        elif self.zero_to == "min1":
            X = np.where(mask, X - 1, X)
        return X

    def inverse_transform(self, X, y=None):
        if self.errors == "sign":
            X = np.sign(X) * 10 ** np.abs(X)
        else:
            X = 10**X
        return X


class PrecisionLimiter(BaseEstimator, TransformerMixin):
    """Limit the number of significant number to precision.

    Keyword Arguments:
    df -- DataFrame (modified inplace)
    cols -- subset of columns to which apply the modification
    precision -- number of significant numbers
    """

    def __init__(self, columns=None, precision=2, mode="floor"):
        store_attr()
        assert self.mode in ["floor", "round"]

    def fit(self, X, y=None):
        if isinstance(X, pd.DataFrame) and not self.columns:
            self.columns = list(X.columns)
        return self

    def transform(self, X, y=None):
        if isinstance(X, pd.DataFrame):
            X = X[self.columns]

        nans = np.isnan(X)
        X[nans] = 0

        base = np.where(X == 0, 1.0, np.abs(X))
        base = np.floor(np.log10(base))
        base -= self.precision - 1
        base = np.power(10, base)

        fct = partial(np.around, decimals=0) if self.mode == "round" else np.floor
        X = fct(X / base).astype(int) * base

        X = np.where(nans, np.nan, X)
        return X


class Mapper(BaseEstimator, TransformerMixin):
    def __init__(self, mapper, main_col):
        assert isinstance(mapper, dict)
        store_attr()

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        if isinstance(X, pd.Series):
            return X.map(self.mapper)
        elif isinstance(X, dict):
            return self.mapper.get(X.get(self.main_col))
        elif isinstance(X, pd.DataFrame):
            return X[self.main_col].map(self.mapper).to_frame()
        else:
            raise RuntimeError("Data format not implemented")

    def save(self, fn):
        with fn.open("w") as f:
            json.dump(self.mapper, f)


class FrameMapper(Mapper):
    def __init__(self, mapper, fillna=None):
        assert isinstance(mapper, pd.DataFrame)
        store_attr()

    @property
    def main_cols(self):
        if isinstance(self.mapper.index, pd.MultiIndex):
            return list(self.mapper.index.names)
        else:
            return listify(self.mapper.index.name)

    def transform(self, X, y=None):
        if isinstance(X, dict):
            try:
                return (
                    self.mapper.loc[(X[col] for col in self.main_cols)]
                    .iloc[0]
                    .to_dict()
                )
            except KeyError:
                return (
                    self.fillna
                    if isinstance(self.fillna, dict)
                    else {c: self.fillna for c in self.mapper.columns}
                )

        if isinstance(X, pd.DataFrame):
            X = X[self.main_cols]
            if isinstance(X, pd.DataFrame):
                out = pd.merge(
                    X, self.mapper, left_on=self.main_cols, right_index=True, how="left"
                )
                out = pd.merge(
                    X,
                    self.mapper,
                    left_on=self.main_cols,
                    right_index=True,
                    how="left",
                )

            if self.fillna is not None:
                out = out.fillna(self.fillna)

            return out.drop(columns=self.main_cols)

        raise RuntimeError(f"Unknown format : {type(X)}")

    def save(self, fn):
        self.mapper.to_csv(fn)

    def get_feature_names(self):
        return list(self.mapper.columns)

    @classmethod
    def from_csv(self, fn, main_cols, **kwargs):
        mapper = pd.read_csv(fn, **kwargs).set_index(listify(main_cols))
        return self(mapper)


class CSVMapper(Mapper):
    def __init__(self, fn, **kwargs):
        self.fn = fn
        super().__init__(*self.mapper_from_csv(fn, **kwargs))

    @classmethod
    def mapper_from_csv(self, fn, **kwargs):
        matching = pd.read_csv(fn, **kwargs)
        main_col = sorted(set(matching.columns) - {"value"})
        if not main_col:
            raise KeyError("Missing main_col in csv")
        main_col = main_col[0]
        matching = {k: v for k, v in matching[[main_col, "value"]].values}
        return matching, main_col


class JSONMapper(Mapper):
    def __init__(self, fn, main_col):
        self.fn = fn
        super().__init__(self.mapper_from_json(fn), main_col)

    @classmethod
    def mapper_from_json(self, fn):
        with open(fn) as f:
            mapper = json.load(f)
        return mapper


class DateFeaturer(BaseEstimator, TransformerMixin):
    def __init__(self, time=False, attrs=None):
        """Helper function that adds columns relevant to a date in the column `field_name` of `df`.
        Code stolen from https://docs.fast.ai/tabular.core.html#add_datepart."""
        attrs = sorted(attrs or [])
        store_attr()

        attr = [
            "year",
            "month",
            "quarter",
            "day",
            "dayofweek",
            "dayofyear",
            "is_month_end",
            "is_month_start",
            "is_quarter_end",
            "is_quarter_start",
            "is_year_end",
            "is_year_start",
            "elapsed",
            "cos_dayinyear",
            "sin_dayinyear",
        ]
        if self.time:
            attr += ["hour", "minute", "second"]
        self.attrs = self.attrs or attr
        self.attr_fct = {
            "week": self._week,
            "elapsed": self._elapsed,
            "cos_dayinyear": partial(self._cos_dayinyear, fct=np.cos),
            "sin_dayinyear": partial(self._cos_dayinyear, fct=np.sin),
        }

    def fit(self, X, y=None):
        self.fields = list(X.columns)
        return self

    def transform(self, X, y=None):
        return pd.concat([self._date_features(X[c]) for c in self.fields])

    def _make_date(self, serie):
        "Make sure `serie` is of the right date type."
        field_dtype = serie.dtype
        if isinstance(field_dtype, pd.core.dtypes.dtypes.DatetimeTZDtype):
            field_dtype = np.datetime64
        if not np.issubdtype(field_dtype, np.datetime64):
            return pd.to_datetime(serie, infer_datetime_format=True)
        return serie

    def _cos_dayinyear(self, serie, fct=np.cos):
        return fct(2 * np.pi * serie.dt.dayofyear / (365 + serie.dt.is_leap_year))

    def _week(self, serie):
        # Pandas removed `dt.week` in v1.1.10
        week = (
            serie.dt.isocalendar().week.astype(serie.dt.day.dtype)
            if hasattr(serie.dt, "isocalendar")
            else serie.dt.week
        )
        return week

    def _elapsed(self, serie):
        mask = ~serie.isna()
        return np.where(mask, serie.values.astype(np.int64) // 10**9, np.nan)

    def _date_features(self, serie):
        serie = self._make_date(serie)
        prefix = re.sub("[Dd]ate$", "", str(serie.name))

        features = {}
        for n in self.attrs:
            try:
                features[prefix + n] = getattr(serie.dt, n)
            except AttributeError:
                features[prefix + n] = self.attr_fct[n](serie)

        return pd.DataFrame(features)

    def get_feature_names(self):
        return [
            re.sub("[Dd]ate$", "", field) + n
            for field in self.fields
            for n in self.attrs
        ]


def filter_num_entries(X, main_cols, num_entries=1):
    g = X.groupby(main_cols).size().pipe(lambda x: x[x >= num_entries])
    g.name = "_size"
    X = pd.merge(X, g, left_on=main_cols, right_index=True, how="right").drop(
        columns=["_size"]
    )
    return X


class AggSerieValue(FrameMapper):
    def __init__(
        self,
        group_cols,
        target,
        date_col="date",
        num_entries=1,
        group_fct=np.sum,
        target_fct=np.median,
        prefix="agg_serie_value_",
        fillna=None,
    ):
        """
        Create a Mapper by aggregating a dataset.

        The dataset in first grouped by main_cols and by date col.
        Then it is aggregated over only main_cols.

        :main_cols:
        Columns identifying features. Groupby is applied on these features

        :target:
        Column to aggregate

        :date_col:
        Column representing the date

        :num_entries:
        Number of minimum elements in a group to apply the agregation

        :group_fct:
        Function to apply when agregating groups per date

        :target_fct:
        Function to apply when aggregating the groups

        :prefix:
        Prefix for the returned columns
        """
        store_attr()
        self.group_cols = listify(self.group_cols)

    def fit(self, X, y=None):
        X = filter_num_entries(X, self.group_cols, self.num_entries)
        self.mapper = (
            X.groupby(self.group_cols + [self.date_col])
            .agg({self.target: self.group_fct})
            .reset_index()
            .groupby(self.group_cols)
            .agg({self.target: listify(self.target_fct)})
        )
        if isinstance(self.mapper.columns, pd.MultiIndex):
            self.mapper.columns = flatten_multiindex(self.mapper.columns)
        self.mapper.columns = [
            "_".join([self.prefix] + self.group_cols + [c]) for c in self.mapper.columns
        ]
        return self


class DateCorrelationEmbedding(FrameMapper):
    def __init__(
        self,
        main_cols,
        target,
        col_date="date",
        num_entries=12,
        umap_opts={},
        pivot_opt={},
        fillna=None,
    ):
        store_attr(["num_entries", "fillna"])

        default_umap_opts = dict(
            n_neighbors=15,
            n_components=2,
            metric="precomputed",
            min_dist=0.1,
            random_state=42,
        )
        default_umap_opts.update(umap_opts)
        self.red = UMAP(**default_umap_opts)

        self.pivot_opt = pivot_opt
        self.pivot_opt.update(
            dict(
                index=col_date,
                columns=main_cols,
                values=target,
                aggfunc="sum",
                fill_value=0,
            )
        )

    def fit(self, X, y=None):
        main_cols = self.pivot_opt["columns"]
        X = filter_num_entries(X, main_cols, self.num_entries)
        table = pd.pivot_table(X, **self.pivot_opt)
        corr, _ = spearmanr(table)
        out = self.red.fit_transform(1 - np.abs(corr))
        self.mapper = pd.DataFrame(
            out,
            columns=[
                "emb_correlation_{}_{}".format("_".join(main_cols), i)
                for i in range(self.red.n_components)
            ],
            index=table.columns,
        )

        return self


def fastai_embedding_to_mapper(df, col_dtype):
    main_col = [x for x in df.columns if "_emb" not in x][0]
    mapper = df.iloc[1:].astype({main_col: col_dtype}).set_index(main_col)

    encoder = FrameMapper(mapper, main_col)
    return encoder
