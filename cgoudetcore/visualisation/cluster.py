import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colormaps as cm
from sklearn.metrics import silhouette_samples
from sklearn.utils import check_array


class ScatterNDim:
    def __init__(self):
        self.fig = None
        self.ax = None

    def _init_canvas(self, data: np.ndarray):
        self.ndim = data.shape[1]
        assert self.ndim > 1

        self.fig, self.ax = plt.subplots(
            nrows=self.ndim - 1,
            ncols=self.ndim - 1,
            facecolor=(1, 1, 1),
            figsize=(6 * self.ndim - 1, 5 * self.ndim - 1),
        )

    def _init_axes_labels(self, axes_labels: list[str] | None = None):
        axes_labels = self._format_axes_labels(axes_labels)

        if self.ndim == 2:
            self.ax.set_xlabel(axes_labels[0])
            self.ax.set_ylabel(axes_labels[1])
            return

        for idim in range(self.ndim - 1):
            for jdim in range(idim + 1, self.ndim):
                self.ax[idim][jdim - 1].set_xlabel(axes_labels[idim])
                self.ax[idim][jdim - 1].set_ylabel(axes_labels[jdim])

    def _format_axes_labels(self, axes_labels: list[str] | None = None) -> list[str]:
        if axes_labels is not None:
            assert len(axes_labels) == self.ndim
        else:
            axes_labels = [f"dim_{i}" for i in range(self.ndim)]
        return axes_labels

    def draw(
        self, data: np.ndarray, dimensions_name: list[str] | None = None, **kwargs
    ):
        data = check_array(data, ensure_2d=True)
        if self.fig is None or self.ax is None:
            self._init_canvas(data)
            self._init_axes_labels(dimensions_name)
        self._draw(data, **kwargs)

    def _draw(self, data: np.ndarray, **kwargs):
        if self.ndim == 2:
            self._draw_on_axis(self.ax, data, **kwargs)
            return

        for idim in range(self.ndim - 1):
            for jdim in range(idim + 1, self.ndim):
                self._draw_on_axis(
                    self.ax[idim][jdim - 1], data[:, [idim, jdim]], **kwargs
                )

    def _draw_on_axis(self, ax: plt.Axes, data: np.ndarray, **kwargs):
        self.mappable = ax.scatter(*data.T, **kwargs)

    def legend(self):
        ax = self.ax if self.ndim == 2 else self.ax[0][-1]
        pos = ax.get_position()
        ax.set_position([pos.x0, pos.y0, pos.width, pos.height])
        ax.legend(loc="upper left", bbox_to_anchor=(self.ndim - 1 + 0.01, 1))
        ax.legend()


def draw_clusters(
    data: np.ndarray, labels: np.ndarray, **kwargs
) -> tuple[plt.Figure, plt.Axes]:
    """
    Draw a scatter plot where each point has a dedicated color defined by a label.

    :param data:
    2D vector with point coordinates

    :param labels:
    Indicator of the group in which belong each data point.
    """
    data = check_array(data, ensure_2d=True)
    labels = check_array(labels, dtype=None, ensure_2d=False)

    plotter = ScatterNDim()
    for label in sorted(np.unique(labels)):
        mask = labels == label
        plotter.draw(
            data[mask], alpha=max(0.3, min(1, 30 / mask.sum())), label=label, **kwargs
        )
    plotter.legend()
    return plotter.fig, plotter.ax


def draw_silhouette(
    data: np.ndarray, labels: np.ndarray
) -> tuple[plt.Figure, plt.Axes]:
    """
    Draw a graph where each data point is represented with its silhouette score.
    Data points are grouped by label then by decreasing score.

    :param data:
    2D vector with point coordinates

    :param labels:
    Indicator of the group in which belong each data point.
    """
    data = check_array(data, ensure_2d=True)
    labels = check_array(labels, dtype=None, ensure_2d=False)
    silhouette_values = silhouette_samples(data, labels)
    return draw_from_silhouette_values(silhouette_values, labels)


def draw_from_silhouette_values(silhouette_values: np.ndarray, labels: np.ndarray):
    silhouette_values = check_array(silhouette_values, ensure_2d=False)
    labels = check_array(labels, dtype=None, ensure_2d=False)

    cluster_labels = np.unique(labels)
    n_clusters = len(cluster_labels)

    fig, ax = plt.subplots(facecolor=(1, 1, 1))
    ax.set_ylim([0, len(silhouette_values) + (n_clusters + 1) * 10])
    ax.get_yaxis().set_visible(False)
    ax.spines.left.set_visible(False)
    ax.set_xlim([min(0, silhouette_values.min()), 1])
    ax.set_xlabel("silhouette score")

    y_lower = 10
    for label in sorted(cluster_labels):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = silhouette_values[labels == label]
        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i
        ax.fill_betweenx(
            np.arange(y_lower, y_upper),
            0,
            ith_cluster_silhouette_values,
            alpha=0.7,
        )

        # Label the silhouette plots with their cluster numbers at the middle
        ax.text(-0.05, y_lower + 0.5 * size_cluster_i, str(label))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples
    return fig, ax


def draw_zscatter(
    data: np.ndarray, zdata: np.ndarray, *, zname: str | None = None
) -> tuple[plt.Figure, plt.Axes]:
    """
    Draw the N dimensional representation of the data with a Z-color bar using the zname column.
    """
    data = check_array(data, ensure_2d=True)
    zdata = check_array(zdata, dtype=None, ensure_2d=False)

    plotter = ScatterNDim()
    cmap = cm.get_cmap("RdYlBu")
    plotter.draw(data, c=zdata, alpha=0.5, cmap=cmap)
    cb = plotter.fig.colorbar(plotter.mappable, ax=plotter.ax)
    cb.set_label(zname)
    return plotter.fig, plotter.ax
