import itertools

import matplotlib.pyplot as plt
import pandas as pd


class SplittedSeriePlotter:
    def __init__(
        self,
        date_column: str,
        features: list[str],
        vertical_split_column: str | None = None,
        horizontal_split_column: str | None = None,
        figsize: tuple[float] | None = None,
    ):
        self.date_column = date_column
        self.features = features
        self.vertical_split_column = vertical_split_column
        self.horizontal_split_column = horizontal_split_column
        self.figsize = figsize

    def plot(self, df: pd.DataFrame) -> tuple[plt.Figure, plt.Axes]:
        self.frame = df
        self.define_segments()
        self.create_axes()
        self.split_dataset()
        self.split_drawing_axes()
        self.split_titles()
        self.draw_series()
        return self.fig, self.ax

    def define_segments(self):
        self.vertical_segments = sorted(
            self.frame[self.vertical_split_column].unique()
            if self.vertical_split_column
            else []
        )

        self.horizontal_segments = sorted(
            self.frame[self.horizontal_split_column].unique()
            if self.horizontal_split_column
            else []
        )

    def create_axes(self):
        nrows = max(len(self.vertical_segments), 1)
        ncols = max(len(self.horizontal_segments), 1)
        default_figsize = (8 * min(ncols, 2), 6 * nrows)
        self.fig, self.ax = plt.subplots(
            nrows=nrows,
            ncols=ncols,
            figsize=self.figsize or default_figsize,
            facecolor=(1, 1, 1, 1),
        )

    def split_dataset(self):
        splitted_frames = [[self.frame]]

        if self.vertical_segments:
            splitted_frames = [
                [self.frame[self.frame[self.vertical_split_column] == value]]
                for value in self.vertical_segments
            ]

        if self.horizontal_segments:
            splitted_frames = [
                [
                    df[df[self.horizontal_split_column] == value]
                    for value in self.horizontal_segments
                ]
                for horizontal in splitted_frames
                for df in horizontal
            ]
        self.splitted_frames = list(itertools.chain(*splitted_frames))

    def split_drawing_axes(self):
        if len(self.vertical_segments) > 1 and len(self.horizontal_segments) > 1:
            self.sub_axes = list(itertools.chain(*self.ax))
        elif len(self.vertical_segments) <= 1 and len(self.horizontal_segments) <= 1:
            self.sub_axes = [self.ax]
        else:
            self.sub_axes = list(itertools.chain(self.ax))

    def split_titles(self):
        titles = [[""]]

        if self.vertical_segments:
            titles = [
                [f"{self.vertical_split_column}={value}; "]
                for value in self.vertical_segments
            ]

        if self.horizontal_segments:
            titles = [
                [
                    t[0] + f"{self.horizontal_split_column}={value}"
                    for value in self.horizontal_segments
                ]
                for t in titles
            ]
        self.titles = list(itertools.chain(*titles))

    def draw_series(self):
        for i, (drawing_axis, to_plot, title) in enumerate(
            zip(self.sub_axes, self.splitted_frames, self.titles, strict=True)
        ):
            self.draw_single_plot(drawing_axis, to_plot, title, label=not i)
        self.fig.legend()

    def draw_single_plot(
        self, drawing_axis: plt.Axes, to_plot: pd.DataFrame, title: str, label: bool
    ):
        alpha = max(0.3, 1 / len(self.features))
        drawing_axis.set_title(title)
        for column in self.features:
            drawing_axis.plot(
                to_plot[self.date_column],
                to_plot[column],
                alpha=alpha,
                label=column if label else None,
            )
