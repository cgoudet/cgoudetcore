import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import shap

from cgoudetcore.learning import compute_shap_values


def shap_plot(learner, dataset: pd.DataFrame):
    values = compute_shap_values(learner, dataset)
    shap.summary_plot(values, dataset, show=False)


def linear_coef_plot(coefs: pd.Series) -> tuple[plt.Figure, plt.Axes]:
    coefs = coefs.sort_values(ascending=False)
    fig, ax = plt.subplots(figsize=(8, max(6, 0.2 * len(coefs))), facecolor=(1, 1, 1))
    ax.barh(coefs.index, coefs.to_numpy())
    return fig, ax


class GroupPerformancePlot:
    """
    Generates a graph which represent for each group the performance of a model with respect to a baseline.
    Each group can be further colored into segments.
    """

    def __init__(
        self,
        performance_col: str = "preds",
        baseline_col: str = "baseline",
        color_col: str | None = None,
    ):
        self._performance_col = performance_col
        self._baseline_col = baseline_col
        self._color_col = color_col

    def plot(self, df: pd.DataFrame) -> tuple[plt.Figure, plt.Axes]:
        df = df.sort_values(self._baseline_col)
        fig, ax = plt.subplots(figsize=(16, 6))
        ys = np.arange(len(df))
        ax.plot(
            ys,
            df[self._baseline_col],
            color="lightgrey",
            label="baseline",
        )

        opts = {"alpha": 0.6}
        if self._color_col is None:
            ax.scatter(ys, df[self._performance_col], **opts)
        else:
            for market_type in sorted(df[self._color_col].dropna().unique()):
                mask = df[self._color_col] == market_type
                ax.scatter(
                    ys[mask],
                    df.loc[mask, self._performance_col],
                    label=market_type,
                    **opts
                )
        ax.set_ylim(0, None)
        ax.set_ylabel(self._performance_col)
        ax.set_xlabel(df.index.name or "; ".join(df.index.names or []))
        fig.legend()
        return fig, ax
