import importlib
import json
from pathlib import Path
from typing import Optional

import numpy as np
import pandas as pd
import shap
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.compose import ColumnTransformer
from sklearn.linear_model import ElasticNet
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from .utils import FeaturesRatio


def init_model(model_name: str, opts: Optional[dict] = None):
    """
    General entrypoint to initialise a machine learning model from configuration.
    """
    with (Path(__file__).parent / "references" / "default_models.json").open() as f:
        model = json.load(f).get(model_name, {"model_type": model_name, "opts": {}})
    model["opts"].update(opts or {})

    model_types = {
        "RandomForestClassifier": ("sklearn.ensemble", "RandomForestClassifier"),
        "RandomForestRegressor": ("sklearn.ensemble", "RandomForestRegressor"),
        "LGBMClassifier": ("lightgbm", "LGBMClassifier"),
        "LinearRegression": ("sklearn.linear_model", "LinearRegression"),
        "LGBMRegressor": ("lightgbm", "LGBMRegressor"),
        "CatBoostRegressor": ("catboost", "CatBoostRegressor"),
        "XGBRegressor": ("xgboost", "XGBRegressor"),
        "XGBRFRegressor": ("xgboost", "XGBRFRegressor"),
        "ColumnRegressor": (__name__, "ColumnRegressor"),
        "ZeroRegressor": (__name__, "ZeroRegressor"),
        "IncreaseRegressor": (__name__, "IncreaseRegressor"),
        "NormalizedRegressor": (__name__, "NormalizedRegressor"),
        "PipelineLinearRegressor": (__name__, "PipelineLinearRegressor"),
        "MultiCategoryRegressor": (__name__, "MultiCategoryRegressor"),
        "UMAP": ("umap", "UMAP"),
        "PCA": ("sklearn.decomposition", "PCA"),
        "HDBSCAN": ("hdbscan", "HDBSCAN"),
        "GaussianMixture": ("sklearn.mixture", "GaussianMixture"),
        "TSNE": ("sklearn.manifold", "TSNE"),
    }
    try:
        module_name, learner = model_types[model["model_type"]]
        return getattr(importlib.import_module(module_name), learner)(**model["opts"])
    except ImportError as e:
        raise RuntimeError(
            f"Please install library {module_name.split('.')[0]} for model {model_name}"
        ) from e
    except KeyError as e:
        options = ",".join(sorted(model_types.keys()))
        raise RuntimeError(f"Unknown model name. Must be in ({options})") from e


def fit_params(
    features, model, X_valid=None, y_valid=None, w_valid=None, categories=None
):
    """
    Parameters to provide to the `fit` function of models for various purposes (depending on underlying model).
    """
    if categories is None:
        categories = []

    cat_idx = [i for i, name in enumerate(features) if name in categories]
    if model.__class__.__name__ == "CatBoostRegressor":
        # catboost is not mandatory to production .
        # require the module only if actually using this module.
        from catboost import Pool

        return {
            "cat_features": cat_idx,
            "eval_set": Pool(X_valid, y_valid, weight=w_valid, cat_features=cat_idx),
        }
    elif model.__class__.__name__ in ("LGBMRegressor", "LGBMClassifier"):
        import lightgbm as lgb

        return {
            "categorical_feature": cat_idx,
            "callbacks": [lgb.log_evaluation()],
            "eval_sample_weight": [w_valid],
            "eval_set": [(X_valid, y_valid)],
        }

    elif model.__class__.__name__ in ("XGBRegressor", "XGBRFRegressor"):
        return {
            "sample_weight_eval_set": [w_valid],
            "eval_set": [(X_valid, y_valid)],
        }
    elif model.__class__.__name__ in ("PipelineLinearRegressor"):
        return {"categories": [x for x in categories if x in features]}
    elif hasattr(model, "fit_params"):
        return model.fit_params(
            features=features,
            X_valid=X_valid,
            y_valid=y_valid,
            w_valid=w_valid,
            categories=categories,
        )
    elif isinstance(model, MultiCategoryRegressor):
        return {
            "fit_params": model.split_fit_params(
                features=features,
                X_valid=X_valid,
                y_valid=y_valid,
                w_valid=w_valid,
                categories=categories,
            )
        }
    return {}


def pred_params(model):
    """
    Add the n_jobs parameter aimed to be used at prediction time for specific models.
    """
    if model.__class__.__name__ in ("LGBMRegressor", "LGBMClassifier"):
        return {"n_jobs": -1}

    elif isinstance(model, TransformRegressor):
        return model.pred_params()

    elif isinstance(model, MultiCategoryRegressor):
        return {
            "pred_params": {
                category: pred_params(
                    model=learner,
                )
                for category, learner in model.models_.items()
            }
        }
    return {}


class ColumnRegressor(BaseEstimator, RegressorMixin):
    """
    Simple regressor that uses a simple feature as its prediction.
    """

    def __init__(self, feature: Optional[list]):
        if not isinstance(feature, list):
            feature = [feature]
        self.feature_name_ = feature

    def __repr__(self):
        return f"ColumnRegressor({self.feature_name_[0]})"

    def fit(self, X, y=None, sample_weight=None):
        return self

    def predict(self, X, y=None, sample_weight=None):
        if isinstance(X, pd.DataFrame):
            return X[self.feature_name_[0]].to_numpy()
        else:
            return X[:, self.feature_name_[0]]


class ZeroRegressor(BaseEstimator, RegressorMixin):
    def __repr__(self):
        return "ZeroRegressor()"

    def fit(self, X, y=None, sample_weight=None):
        return self

    def predict(self, X, y=None, sample_weight=None):
        return np.zeros(len(X))


class TransformRegressor(BaseEstimator, RegressorMixin):
    """
    Hold a model and transformations to predict intermediate results.
    """

    def __init__(
        self,
        model_name: str,
        reference: str,
        model_opt: Optional[dict] = None,
        drop_reference: bool = False,
    ):
        self.modelopt = model_opt or {}
        self.modelname = model_name
        self.model = init_model(model_name, self.modelopt)
        self.reference = reference
        self.drop_reference = drop_reference

    def __repr__(self):
        return f"{self.__class__.__name__}(model_name={self.modelname}, reference={self.reference}, model_opt={self.modelopt})"

    def fit(self, X, y=None, sample_weight=None, **kwargs):
        X, y, sample_weight = self._transform(X, y, sample_weight)
        self.model.fit(X, y, sample_weight=sample_weight, **kwargs)
        return self

    def fit_params(
        self, features, X_valid=None, y_valid=None, w_valid=None, categories=None
    ):
        X_valid, y_valid, w_valid = self._transform(X_valid, y_valid, w_valid)
        if self.drop_reference:
            features = [f for f in features if f != self.reference]
        return fit_params(features, self.model, X_valid, y_valid, w_valid, categories)

    def _transform(self, X, y, sample_weight=None):
        return (
            self._transform_X(X),
            self._transform_target(X, y),
            self._transform_weight(X, sample_weight),
        )

    def _transform_X(self, X):
        return X

    def _transform_target(self, X, y=None):
        return y

    def _transform_weight(self, X, sample_weight=None):
        return sample_weight

    def predict(self, X, y=None, **kwargs):
        X_predict = self._transform_X(X)
        raw_preds = self.model.predict(X_predict, **kwargs)
        return self._inverse_target(X, raw_preds)

    def _inverse_target(self, X, preds):
        return preds

    def pred_params(self):
        return pred_params(self.model)


class NormalizedRegressor(TransformRegressor):
    """
    Predict the target divided by the reference
    """

    def __init__(
        self,
        model_name: str,
        reference: str,
        model_opt: Optional[dict] = None,
        drop_reference: bool = False,
        **kwargs,
    ):
        super().__init__(
            model_name, reference, model_opt=model_opt, drop_reference=drop_reference
        )
        self.feature_ratio = FeaturesRatio(
            num_col="target", denom_col=reference, **kwargs
        )

    def _inverse_target(self, X, preds):
        return preds * X[self.reference]

    def _transform_weight(self, X, sample_weight=None):
        if sample_weight is not None:
            sample_weight = sample_weight * X[self.reference]
        return sample_weight

    def _transform_target(self, X, y=None):
        if y is not None:
            y = self.feature_ratio.run(X.assign(target=y))
            assert not np.isinf(y.max())
        return y

    def _transform_X(self, X):
        if self.drop_reference:
            X = X.drop(columns=self.reference)
        return X


class PipelineLinearRegressor(BaseEstimator, RegressorMixin):
    def __init__(
        self, model_params: Optional[dict] = None, onehot_params: Optional[dict] = None
    ):
        self.modelparams = model_params or {}
        self.onehot_params = {"sparse_output": False, **(onehot_params or {})}

    @property
    def model(self):
        if not hasattr(self, "pipeline_"):
            return None
        return self.pipeline_.named_steps["model"]

    def fit(self, X, y=None, sample_weight=None, categories: Optional[list] = None):
        if categories is None:
            categories = []
        tf = ColumnTransformer(
            [("onehot", OneHotEncoder(**self.onehot_params), categories)],
            remainder="passthrough",
        )
        self.pipeline_ = Pipeline(
            [("preproc", tf), ("model", ElasticNet(**self.modelparams))]
        )
        self.pipeline_.fit(X, y, model__sample_weight=sample_weight)
        return self

    def predict(self, X, y=None, sample_weight=None):
        return self.pipeline_.predict(X)

    def coefs(self):
        """
        Return the ordered list of fitted coefficients.
        """
        values = list(self.model.coef_) + [self.model.intercept_]
        labels = [
            x.replace("remainder__", "")
            for x in self.pipeline_.named_steps["preproc"].get_feature_names_out()
        ] + ["intercept"]
        return pd.Series(values, index=labels).sort_values()


class MultiCategoryRegressor(BaseEstimator, RegressorMixin):
    """
    Split the dataset in multiple categories and train per-category models.
    """

    def __init__(self, configs: dict[str:any]):
        self.config_ = configs
        self.models_ = {
            filter_column: init_model(**model_params)
            for filter_column, model_params in configs.items()
        }

    def fit(self, X, y=None, sample_weight=None, fit_params=None, **kwargs):
        fit_params = fit_params or {}
        subsamples = self._categories_subsamples(X, y, sample_weight)
        for filter_column, learner in self.models_.items():
            to_train, to_learn, to_weight = subsamples[filter_column]
            learner.fit(
                to_train,
                to_learn,
                sample_weight=to_weight,
                **fit_params.get(filter_column, {}),
            )
        return self

    def split_fit_params(
        self, features, X_valid=None, y_valid=None, w_valid=None, categories=None
    ):
        subsamples = self._categories_subsamples(X_valid, y_valid, w_valid)

        params = {}
        for category, learner in self.models_.items():
            to_train, to_learn, to_weight = subsamples[category]
            params[category] = fit_params(
                features=features,
                model=learner,
                X_valid=to_train,
                y_valid=to_learn,
                w_valid=to_weight,
                categories=categories,
            )
        return params

    def _categories_subsamples(self, X, y, sample_weight=None):
        features_for_masks, features_for_train = self._split_mask_train_features(X)

        samples = {}
        for filter_column in self.models_:
            mask = features_for_masks[filter_column]

            samples[filter_column] = (
                features_for_train[mask],
                y[mask],
                sample_weight[mask] if sample_weight is not None else None,
            )
        return samples

    def _split_mask_train_features(self, X):
        features_for_masks = X[[c for c in self.models_ if c != "remaining_"]].astype(
            bool
        )

        if "remaining_" in self.models_:
            features_for_masks["remaining_"] = ~features_for_masks.sum(axis=1).astype(
                bool
            )
        features_for_train = X.drop(columns=self.models_.keys(), errors="ignore")
        return features_for_masks, features_for_train

    def predict(self, X, y=None, pred_params=None):
        pred_params = pred_params or {}
        preds = np.ones(len(X)) * np.nan
        features_for_masks, features_for_preds = self._split_mask_train_features(X)
        for filter_column, learner in self.models_.items():
            mask = features_for_masks[filter_column].to_numpy()
            preds[mask] = learner.predict(
                features_for_preds[mask],
                **pred_params.get(filter_column, {}),
            )
        return preds


def transform_dataset(
    train_data: pd.DataFrame, transformer_type: str, transformer_params: dict
):
    transformer = init_model(transformer_type, transformer_params)
    transformed_data = transformer.fit_transform(train_data)
    return pd.DataFrame(
        transformed_data,
        index=train_data.index,
        columns=[f"emb_{i}" for i in range(transformed_data.shape[1])],
    )


def compute_shap_values(learner, dataset: pd.DataFrame):
    explainer = shap.TreeExplainer(learner)
    return explainer.shap_values(dataset)


def is_shap_explicable(learner) -> bool:
    try:
        shap.TreeExplainer(learner)
        return True
    except Exception:
        return False
