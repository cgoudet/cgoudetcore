import datetime
import json
import logging
import re
import subprocess
import tempfile
from dataclasses import dataclass
from logging.handlers import RotatingFileHandler
from pathlib import Path
from typing import Optional

import joblib
import numpy as np
import pandas as pd
from pythonjsonlogger import jsonlogger

from . import ROOTDIR


def git_hash():
    return subprocess.check_output(["git", "rev-parse", "HEAD"]).strip().decode("ascii")


def json_categorify(df: pd.DataFrame, column: str, outdir: Path) -> pd.DataFrame:
    """
    Encode a categorical variable as an integer, by saving the vocabulary as a json.
    """
    outfn = outdir / f"mapping_{column}.json"
    if outfn.exists():
        with outfn.open() as f:
            mapping = json.load(f)
    else:
        mapping = {c: i for i, c in enumerate(sorted(df[column].dropna().unique()))}
        with outfn.open("w") as f:
            json.dump(mapping, f)
    return df.assign(**{column: df[column].map(mapping).fillna(-1).astype(int)})


def categorify_columns(sales, columns: list, outdir: Path):
    """
    Encode multiple categorical variables as integers.
    """
    sales = sales.astype({c: str for c in columns})
    for c in columns:
        sales = sales.pipe(json_categorify, c, outdir)
    return sales


def category_to_json(serie):
    return {k: i for i, k in enumerate(sorted(serie.unique()))}


def flatten_multiindex(index, sep="_"):
    if not isinstance(index, pd.MultiIndex):
        return index
    name = sep.join(index.names) if [x for x in index.names if x is not None] else None
    return pd.Index([sep.join([str(el) for el in ind]) for ind in index], name=name)


def reduce_types(df):
    return df.pipe(reduce_float_type).pipe(reduce_int_type)


def reduce_float_type(df):
    float_cols = list(df.select_dtypes("float64").columns)
    return df.astype({c: np.float32 for c in float_cols})


def reduce_int_type(df):
    int_cols = list(df.select_dtypes("integer").columns)
    encodings = [np.int8, np.int16, np.int32]

    maxes = df[int_cols].abs().max()
    for c in int_cols:
        m = maxes[c]
        for e in encodings:
            if m < np.iinfo(e).max:
                df[c] = df[c].astype(e)
                break
    return df


def check_request_status(r):
    if r.status_code != 200:
        print(r.url)
        print(r.status_code)
        print(r.content)
        raise RuntimeError(
            str(r.status_code) + " " + r.content.decode("utf-8") + " " + r.url
        )


def get_logger(fn):
    logger = logging.getLogger("walmart")
    logger.setLevel(20)
    handler = RotatingFileHandler(fn, maxBytes=100 * 8 * 2 ** (10 * 2), backupCount=5)
    formatter = JsonFormatter(fmt="(levelname) $(message)")
    handler.setFormatter(formatter)
    handler.setLevel(20)
    logger.addHandler(handler)
    return logger


class JsonFormatter(jsonlogger.JsonFormatter):
    def __init__(
        self,
        fmt="%(asctime) %(name) %(processName) %(filename)  %(funcName) %(levelname) %(lineno) %(module) %(threadName) %(message)",
        datefmt="%Y-%m-%dT%H:%M:%SZ%z",
        style="%",
        extra={},
        *args,
        **kwargs,
    ):
        self._extra = extra
        jsonlogger.JsonFormatter.__init__(
            self, fmt=fmt, datefmt=datefmt, *args, **kwargs
        )

    def process_log_record(self, log_record):
        # Enforce the presence of a timestamp
        if "asctime" in log_record:
            log_record["timestamp"] = log_record["asctime"]
        else:
            log_record["timestamp"] = datetime.datetime.utcnow().strftime(
                "%Y-%m-%dT%H:%M:%S.%fZ%z"
            )

        if self._extra is not None:
            for key, value in self._extra.items():
                log_record[key] = value
        return super().process_log_record(log_record)


def addl(l1, l2):
    return sorted(set(l1) | set(l2))


def diffl(l1, l2):
    return sorted(set(l1) - set(l2))


def check_missing_cols(df, columns):
    missings = diffl(columns, df.columns)
    if missings:
        raise KeyError(f"Missing columns : {missings}")


def check_missings(df, columns=None):
    if isinstance(df, pd.DataFrame):
        columns = columns or df.columns
        check_missing_cols(df, columns)

        missings = df.loc[:, columns].isnull().mean(0)
        missings = missings[missings > 0]
        if len(missings):
            print(missings)
            raise RuntimeError("missing values")
    elif isinstance(df, pd.Series):
        missings = df[df.isnull()]
        if len(missings):
            print(missings)
            raise RuntimeError("missing values")


def multi_replace(string, replacements):
    """Given a string and a replacement map, it returns the replaced string.

    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}

    :return: modified string
    :rtype: str

    This code is an adaptation of :
    https://gist.github.com/bgusach/a967e0587d6e01e889fd1d776c5f3729
    """
    if not len(replacements):
        return string

    # Place longer ones first to keep shorter substrings from matching where the longer
    # ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string
    # 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    substrs = sorted(replacements, key=len, reverse=True)

    # Create a big OR regex that matches any of the substrings to replace
    regexp = re.compile("|".join(map(re.escape, substrs)))

    # For each match, look up the new string in the replacements
    return regexp.sub(lambda match: replacements[match.group(0)], string)


def pickle_size(model):
    with tempfile.TemporaryDirectory() as tmpdirname:
        fn = Path(tmpdirname) / "model.joblib"
        joblib.dump(model, fn)
        return fn.stat().st_size


def read_external(d, key, attr="features"):
    val = d.get(key)
    if val is None:
        return val

    if isinstance(val, list) or isinstance(val, dict):
        return val

    if isinstance(val, str):
        val = Path(val)

    if not isinstance(val, Path):
        raise RuntimeError(f"Wrong input format : {type(val)}")

    if val.suffix == ".joblib":
        clf = joblib.load(Path(val))
        return getattr(clf, attr)

    if val.suffix == ".txt":
        path = Path(val)
        if not path.is_absolute():
            path = ROOTDIR / path
        return list(np.loadtxt(path, dtype=str))

    if val.suffix == ".json":
        with Path(val).open() as f:
            return json.load(f)

    raise RuntimeError(f"Unknown extension : {val.suffix}")


@dataclass
class FeaturesRatio:
    def __init__(
        self,
        num_col: str,
        denom_col: str,
        lower_denom_clip: Optional[float] = None,
        upper_clip: Optional[float] = None,
        is_relative: bool = False,
        zero_by_num: bool = False,
    ):
        self.num_col = num_col
        self.denom_col = denom_col
        self.lower_denom_clip = lower_denom_clip
        self.upper_clip = upper_clip
        self.is_relative = is_relative
        self.zero_by_num = zero_by_num

    def run(self, df: pd.DataFrame) -> pd.Series:
        num = df[self.num_col]
        if self.is_relative:
            num = num.sub(df[self.denom_col])
        denom = df[self.denom_col]
        if self.lower_denom_clip is not None:
            denom = denom.clip(lower=self.lower_denom_clip)
        if self.zero_by_num:
            denom = denom.where(lambda s: s != 0, df[self.num_col])

        final = num.div(denom)
        if self.upper_clip is not None:
            final = final.clip(upper=self.upper_clip)
        return final.where(num != denom, 1)
