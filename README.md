# cgoudetcore

[![coverage report](https://gitlab.com/cgoudet/cgoudetcore/badges/main/coverage.svg)](https://gitlab.com/<project path>/-/commits/main)

To realease a new version.
- git tag -a <version> -m 'message'
- git push --tags
- python3 setup.py sdist
- commit and push
